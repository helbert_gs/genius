﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour
{
    /// <summary>
    /// Cores (Objetos).
    /// </summary>
    [SerializeField]
    private List<Transform> Color = null;

    /// <summary>
    /// Sequência de Cores (Sistema).
    /// </summary>
    [SerializeField]
    private List<int> GameSequence = null;

    /// <summary>
    /// Sequência de Cores (Usuário).
    /// </summary>
    [SerializeField]
    private List<int> UserSequence = null;

    /// <summary>
    /// Tempo de transição.
    /// </summary>
    [SerializeField]
    private float Transition = 0.5f;

    /// <summary>
    /// Tempo esperando a ação do usuário.
    /// </summary>
    [SerializeField]
    private float Delay = 15.0f;

    /// <summary>
    /// Tempo decorrido esperando ação do usuário.
    /// </summary>
    [SerializeField]
    private float Elapsed = 0f;

    /// <summary>
    /// Pontuação.
    /// </summary>
    [SerializeField]
    private UnityEngine.UI.Text Score = null;

    /// <summary>
    /// Nível.
    /// </summary>
    [SerializeField]
    private UnityEngine.UI.Text Level = null;

    /// <summary>
    /// Alerta para o usuário.
    /// </summary>
    [SerializeField]
    private UnityEngine.UI.Image Status = null;

    /// <summary>
    /// Cor padrão do display de alerta.
    /// </summary>
    [SerializeField]
    private UnityEngine.Color ColorDefault;

    /// <summary>
    /// Alerta o usuário a executar a ação.
    /// </summary>
    [SerializeField]
    private UnityEngine.Color ColorAction;

    /// <summary>
    /// Alerta o usuário que o mesmo errou.
    /// </summary>
    [SerializeField]
    private UnityEngine.Color ColorError;

    /// <summary>
    /// Cronometro.
    /// </summary>
    [SerializeField]
    private UnityEngine.UI.Text Timer = null;

    /// <summary>
    /// Cronometro.
    /// </summary>
    public bool UseKinect = true;

    /// <summary>
    /// Ícone de iniciar o jogo com o Kinect..
    /// </summary>
    [SerializeField]
    private UnityEngine.UI.RawImage IconKinect = null;

    /// <summary>
    /// Ícone de iniciar o jogo com o teclado.
    /// </summary>
    [SerializeField]
    private UnityEngine.UI.RawImage IconKeyboard = null;

    /// <summary>
    /// Fim do Jogo.
    /// </summary>
    [SerializeField]
    private bool Stop = false;

    /// <summary>
    /// Determina se é para reiniciar a cena.
    /// </summary>
    [SerializeField]
    private bool Reload = false;

    /// <summary>
    /// Controlador de movimentos.
    /// </summary>
    [SerializeField]
    private PlayerController playerController;

    /// <summary>
    /// Log.
    /// </summary>
    [SerializeField]
    private bool Log = false;

    /// <summary>
    /// Efeito sonoro ao executar ação.
    /// </summary>
    [SerializeField]
    private GameObject BlueSoundEffect;

    /// <summary>
    /// Efeito sonoro ao executar ação.
    /// </summary>
    [SerializeField]
    private GameObject GreenSoundEffect;

    /// <summary>
    /// Efeito sonoro ao executar ação.
    /// </summary>
    [SerializeField]
    private GameObject RedSoundEffect;

    /// <summary>
    /// Efeito sonoro ao executar ação.
    /// </summary>
    [SerializeField]
    private GameObject YellowSoundEffect;
    /// <summary>
    /// Efeito sonoro ao executar ação.
    /// </summary>
    [SerializeField]
    private GameObject ErrorSoundEffect;

    /// <summary>
    /// Executado ao iniciar a cena.
    /// </summary>
    private void Start()
    {
        this.Stop = false;
        this.Reload = false;

        StopAllCoroutines();

        // Desabilita as cores (luzes) ativadas.
        foreach (var element in this.Color)
        {
            element.gameObject.SetActive(false);
            if (this.Log)
                print(element.name + ": Enable = false.");
        }

        // O timer inicia desabilitado.
        this.Timer.gameObject.SetActive(false);

        print(this.Timer.gameObject.name + ": Enable = false.");
    }

    /// <summary>
    /// Executado a cada frame.
    /// </summary>
    private void Update()
    {
        if (this.Reload)
            Application.LoadLevel(Application.loadedLevel);

        if (Input.GetKeyDown(KeyCode.Escape))
            this.Reload = true;
    }

    /// <summary>
    /// Inicia o jogo.
    /// </summary>
    public IEnumerator Play()
    {
        if (this.Log)
            print("Started Game.");

        // Cria uma nova instância do objeto GameSequence.
        this.GameSequence = new List<int>();

        // Cria uma nova instância do objeto UserSequence.
        this.UserSequence = new List<int>();

        yield return new WaitForSeconds(1);

        // Inicia contagem regressiva.
        this.IconKeyboard.gameObject.SetActive(false);
        this.IconKinect.gameObject.SetActive(false);
        this.Timer.gameObject.SetActive(true);

        for (int i = 3; i > 0; i--)
        {
            this.Timer.text = i.ToString();
            yield return new WaitForSeconds(1);
        }

        this.Timer.gameObject.SetActive(false);

        // Enquanto a condição de parada não for satisfeita.
        while (!this.Stop)
        {
            // Exibe a sequência de cores.
            yield return StartCoroutine(GetGameSequence());

            // Espera ação do usuário.
            yield return StartCoroutine(WaitForUserAction());

            // Limpa a sequencia informada pelo usuário.
            this.UserSequence.Clear();
        }
    }

    /// <summary>
    /// Fim de Jogo.
    /// </summary>
    private IEnumerator GameOver()
    {
        StopCoroutine(GetGameSequence());

        this.ErrorSoundEffect.GetComponent<AudioSource>().Play();

        for (int i = 0; i < 3; i++)
        {
            yield return new WaitForSeconds(0.5f);

            this.Status.color = this.ColorDefault;

            this.Score.gameObject.SetActive(false);
            this.Level.gameObject.SetActive(false);

            yield return new WaitForSeconds(0.5f);

            this.Status.color = this.ColorError;

            this.Score.gameObject.SetActive(true);
            this.Level.gameObject.SetActive(true);
        }

        yield return new WaitForSeconds(0.5f);
        this.Status.color = this.ColorDefault;
        this.Score.text = "0";
        this.Level.text = "0";
        this.Stop = false;
        this.IconKinect.gameObject.SetActive(true);
        this.playerController.GameOver();
        this.GameSequence.Clear();
        this.UserSequence.Clear();

        yield return new WaitForSeconds(1);

        this.Reload = true;

        //StopAllCoroutines();
        //Application.LoadLevel(Application.loadedLevel);
    }

    /// <summary>
    /// Exibe a sequência de cores.
    /// </summary>
    private IEnumerator GetGameSequence()
    {
        this.Level.text = (System.Convert.ToInt32(this.Level.text) + 1).ToString();

        // Stop the program for x seconds.
        yield return new WaitForSeconds(this.Transition);

        if (this.Log)
            print("Get Sequence...");

        // Update seed.
        Random.seed = (int)Time.realtimeSinceStartup;

        foreach (var index in this.GameSequence)
        {
            // Light On
            this.Color[index].gameObject.SetActive(true);

            if (this.Log)
                print(this.Color[index].gameObject.name);

            // Stop the program for x seconds.
            yield return new WaitForSeconds(this.Transition);

            // Light Off.
            this.Color[index].gameObject.SetActive(false);

            // Stop the program for x seconds.
            yield return new WaitForSeconds(this.Transition);
        }

        // Random value.
        int r = Random.Range(0, 4);

        // Add col or sequence.
        this.GameSequence.Add(r);

        // Light On
        this.Color[r].gameObject.SetActive(true);

        if (r == 0)
            this.BlueSoundEffect.GetComponent<AudioSource>().Play();

        if (r == 1)
            this.GreenSoundEffect.GetComponent<AudioSource>().Play();

        if (r == 2)
            this.RedSoundEffect.GetComponent<AudioSource>().Play();

        if (r == 3)
            this.YellowSoundEffect.GetComponent<AudioSource>().Play();

        if (this.Log)
            print(this.Color[r].gameObject.name);

        yield return new WaitForSeconds(this.Transition);

        // Light Off.
        this.Color[r].gameObject.SetActive(false);

        // Aumenta o delay.
        this.Delay += 0.5f;

        // Stop the program for x seconds.
        yield return new WaitForSeconds(this.Transition);
    }

    /// <summary>
    /// Espera a ação do usuário.
    /// </summary>
    /// <returns></returns>
    private IEnumerator WaitForUserAction()
    {
        print("Esperando ação do usuário...");

        this.Status.color = this.ColorAction;

        this.Elapsed = 0.0f;

        while (this.Elapsed < this.Delay)
        {
            yield return new WaitForSeconds(0.2f);
            this.Elapsed += (0.2f);
        }

        this.Status.color = this.ColorDefault;

        if (this.GameSequence.Count > this.UserSequence.Count)
        {
            this.Stop = true;
            StartCoroutine(GameOver());
        }
    }

    /// <summary>
    /// Adiciona a cor informada pelo usuário na lista de cores, para relizar a comparação das mesmas.
    /// </summary>
    /// <param name="index">Color.</param>
    public IEnumerator SetUserSequence(int index) 
    {
        if (index == 0)
            this.BlueSoundEffect.GetComponent<AudioSource>().Play();

        if (index == 1)
            this.GreenSoundEffect.GetComponent<AudioSource>().Play();
        
        if (index == 2)
            this.RedSoundEffect.GetComponent<AudioSource>().Play();

        if (index == 3)
            this.YellowSoundEffect.GetComponent<AudioSource>().Play();
        

        // Destaca a cor selecionada pelo usuário.
        this.Color[index].gameObject.SetActive(true);

        // Espera x segundos para continuar o programa.
        yield return new WaitForSeconds(0.2f);

        // Elimina o destaque da cor selecionada pelo usuário.
        this.Color[index].gameObject.SetActive(false);

        // Zera o tempo decorrido desde a ação do usuário.
        this.Elapsed = 0;

        // Adiciona a cor selecionada pelo usuário na sequência de cores informada pelo usuário.
        this.UserSequence.Add(index);

        // Verifica se a cor selecionada pelo usuário é a mesma da sequência informada pelo jogo.
        if (this.GameSequence[this.UserSequence.Count - 1] != this.UserSequence[this.UserSequence.Count - 1])
        {
            // Caso não seja a mesma cor, a condição de parada do jogo é satisfeita.
            this.Stop = true;
            yield return StartCoroutine(GameOver());
        }
        else
        {
            this.Score.text = (System.Convert.ToInt32(this.Score.text) + 1).ToString();
        }

        
    }

    /// <summary>
    /// Informação se usa Kinect.
    /// </summary>
    /// <param name="b"></param>
    public void SetUseKinect(bool b)
    {
        this.UseKinect = b;
    }
}
