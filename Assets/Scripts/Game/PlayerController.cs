﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {


	//Objetos do esqueleto
	public GameObject leftHand;
	public GameObject rightHand;
	public GameObject leftHip;
	public GameObject rightHip;
	public GameObject leftShoulder;
	public GameObject rightShoulder;
	public GameObject head;
	public GameObject torso;

	//Raiz do esqueleto
	public GameObject blockman;

	//Game controller
	private Game game;

	
	//Variaveis de controle das ações do player
	[SerializeField] private Vector3 userInitialPosition;
	[SerializeField] private float handsUpTreshold;
	[SerializeField]private float multiplier;
	private bool gameStarted;
	private bool isPlayerAtCenter;

	// Use this for initialization
	void Start () 
	{
		game = GameObject.Find("3. Game Manager").GetComponent<Game>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!gameStarted && GameStartCondition())
		{
			game.StartCoroutine("Play");
			gameStarted = true;
			userInitialPosition = torso.transform.position;
			isPlayerAtCenter = true;
		}

		if(gameStarted)
		{

			Debug.Log("Torso Delta Z: " + (torso.transform.position.z - userInitialPosition.z).ToString());
			Debug.Log("Torso Delta X: " + (torso.transform.position.x - userInitialPosition.x).ToString());
			//Caso o jogador não volte para o centro, ele não poderá selecionar uma nova cor, evitando assim uma ação de pressionar o mesmo botão diversas vezes
			if(isPlayerAtCenter)
			{
				if(IsBlue())
				{
					StartCoroutine(game.SetUserSequence(0));
					isPlayerAtCenter = false;
				}
				if(IsGreen())
				{
					StartCoroutine(game.SetUserSequence(1));
					isPlayerAtCenter = false;
				}
				if(IsRed())
				{
					StartCoroutine(game.SetUserSequence(2));
					isPlayerAtCenter = false;
				}
					
				if(IsYellow())
				{
					StartCoroutine(game.SetUserSequence(3));
					isPlayerAtCenter = false;
				}
			}else{
				isPlayerAtCenter = !IsBlue() && !IsYellow() && !IsRed() && !IsGreen();
			}

			
		}
	}

	//Condição de inicio do jogo: levantar ambas as mãos acima da cabeça
	bool GameStartCondition()
	{
		return IsAbove(rightHand.transform.position.y, head.transform.position.y) && 
			IsAbove(leftHand.transform.position.y, head.transform.position.y);
	}


	//Controle de referencia de objetos
	
	bool IsAbove(float x1, float x2)
	{ 
		return x1 > x2? true : false;
	}

	bool IsFront(float z1, float z2)
	{
		return z1 > z2 ? true: false;
	}

	bool IsRear(float z1, float z2)
	{
		return z1*10000 > z2*10000 ? true: false;
	}

	private float Delta(float value01, float value02)
	{
		return value01 - value02;
	}

	//Controle das cores
	//TODO: refinar e regular os controles
	bool IsGreen()
	{
		return (Delta(torso.transform.position.x, userInitialPosition.x) < -0.23f &&
		        Delta (torso.transform.position.z, userInitialPosition.z) > 0.30f);
	}

	bool IsRed()
	{
		return (Delta(torso.transform.position.x, userInitialPosition.x) > 0.20f &&
		        Delta(torso.transform.position.z, userInitialPosition.z) > 0.15f);
	}

	bool IsYellow()
	{
		return (Delta(torso.transform.position.x, userInitialPosition.x) < -0.24f &&
		        Delta(torso.transform.position.z, userInitialPosition.z) < -0.30f);
	}

	bool IsBlue()
	{
		return (Delta (torso.transform.position.x, userInitialPosition.x) > 0.25f &&
		        Delta(torso.transform.position.z,userInitialPosition.z) < -0.30f);
	}

	bool IsAtCenter()
	{
		return (Delta (torso.transform.position.x,userInitialPosition.x) < -0.03f &&
		        Delta (torso.transform.position.x,userInitialPosition.x) > -0.10f &&
		   		Delta(torso.transform.position.z, userInitialPosition.z) < -0.02f &&
		        Delta(torso.transform.position.z, userInitialPosition.z) > -0.20f);
	}

	public void GameOver()
	{
		gameStarted = false;
	}
}
