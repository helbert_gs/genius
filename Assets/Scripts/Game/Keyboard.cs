﻿using UnityEngine;
using System.Collections;

public class Keyboard : MonoBehaviour
{
    /// <summary>
    /// Ativa a cor azul.
    /// </summary>
    [SerializeField]
    private KeyCode Blue = KeyCode.A;

    /// <summary>
    /// Ativa a cor verde.
    /// </summary>
    [SerializeField]
    private KeyCode Green = KeyCode.S;

    /// <summary>
    /// Ativa a cor vermelho.
    /// </summary>
    [SerializeField]
    private KeyCode Red = KeyCode.D;

    /// <summary>
    /// Ativa a cor amarelo.
    /// </summary>
    [SerializeField]
    private KeyCode Yellow = KeyCode.F;

    /// <summary>
    /// Gerenciador do Jogo.
    /// </summary>
    [SerializeField]
    private GameObject GameManager = null;

    /// <summary>
    /// Executado a cada frame.
    /// </summary>
    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.Return))
            StartCoroutine(this.GameManager.GetComponent<Game>().Play());

        if (Input.GetKeyDown(this.Blue))
            StartCoroutine(this.GameManager.GetComponent<Game>().SetUserSequence(0));

        if (Input.GetKeyDown(this.Green))
            StartCoroutine(this.GameManager.GetComponent<Game>().SetUserSequence(1));

        if (Input.GetKeyDown(this.Red))
            StartCoroutine(this.GameManager.GetComponent<Game>().SetUserSequence(2));

        if (Input.GetKeyDown(this.Yellow))
            StartCoroutine(this.GameManager.GetComponent<Game>().SetUserSequence(3));
    }
}
